# PROJET FIL ROUGE

## Description

Ce projet est un site simple qui comprend une page d'accueil avec 3 images et des liens vers les autres pages. L'aspect graphique du site est basé sur Bootstrap cards. Il contient également 3 pages dédiées aux technologies découvertes lors du cours : Docker, Ansible et Gitlab CI/CD. Le projet suit une charte graphique et utilise des contenus libres. Une documentation organisée est fournie pour faciliter le développement.

- Le projet est conçu pour fonctionner avec Gitlab CI/CD.

- Ansible est utilisé pour générer un npm (Node.js Package Manager).

- Le pipeline de Gitlab CI/CD est configuré pour générer automatiquement le site HTML.

## Technologies Utilisées

- HTML

- CSS (Bootstrap)

- JavaScript (facultatif, selon les besoins)

- Node JS

## Pages du Site

### Page d'Accueil

- La page d'accueil présente 3 images représentant les technologies du projet.

- Chaque image est associée à un lien vers la page dédiée de la technologie correspondante.

### Page Docker

- La page Docker contient des informations et du contenu relatif à la technologie Docker.

- Des tutoriels, explications et liens utiles sur Docker peuvent être ajoutés ici.

### Page Ansible

- La page Ansible présente du contenu relatif à la technologie Ansible.

- Vous pouvez y inclure des exemples d'utilisation, des explications et des références liées à Ansible.

### Page Gitlab CI/CD

- La page Gitlab CI/CD est dédiée au contenu sur la technologie Gitlab CI/CD.

- Vous pouvez y ajouter des explications, des schémas d'intégration continue et de déploiement continu, ainsi que des liens vers des tutoriels sur Gitlab CI/CD.

## Charte Graphique et Contenus Libres

- Le projet suit une charte graphique spécifique pour maintenir une cohérence visuelle.

- Les contenus utilisés sur le site sont libres de droits pour éviter toute violation de droits d'auteur.

## Auteur

Ce projet a été développé par:

- Franklin NGALEU

- Khartoum SYLLA

- Maxime FRAGNET

- Néoline TAKIDO

## Lancement du Projet

- Suivre ce [lien](https://hub.docker.com/repositories/franklink17) afin d'avoir accès à l'image docker du projet,  

- Lancer la commande suivante sur votre terminal afin de copier l'image dans votre Docker Desktop `docker pull franklink17/projet-final-groupe`

- Afin de créer le conteneur vous lancez cette commande `docker run -dp 127.0.0.1:8085:80 franklink17/projet-final-groupe`

- Vous pouvez ouvrir votre local host afin d'accéder au site.
